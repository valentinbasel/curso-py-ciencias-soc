# Lenguaje de programación PYTHON para científicos sociales


### Duración

Un módulo de 12 clases, 2 horas de duración, una vez a la semana (3 meses)

### Introducción

El uso de lenguajes de programación para automatización de tareas repetitivas permite a lxs investigadorxs liberar parte de su trabajo, dejando estas tareas de búsqueda, limpieza y presentación de datos a PYTHON, así como la capacidad de leer información de páginas webs de forma automatizada.
Actualmente, una gran parte de la información que funciona como materia prima para investigación en ciencias sociales y humanas, tiene su origen en plataformas digitales, desde páginas WEBs (portales de noticias, redes sociales) hasta documentos digitalizados (archivos de textos, PDFs). Toda esta información tiene su propia naturaleza, y a la hora de trabajar con estos sistemas, la capacidad de los lenguajes de programación para poder manipular y procesar grandes volúmenes de datos de forma rápida y automatizada se vuelve una herramienta más que interesante para poder incorporar a un proceso de investigación.
Este curso está pensado para investigadorxs, becarixs y estudiantxs que trabajen en el campo de las ciencias sociales y humanas y que tengan interés en aprender las nociones básicas de trabajo con un lenguaje de programación moderno como PYTHON y cómo incorporarlo para procesar información textual de forma automatizada. El curso pretende al mismo tiempo ofrecer herramientas muy accesibles para el procesamiento de información y formar una incipiente comunidad de usuarixs en el área.
Durante el cursado, utilizaremos el lenguaje de programación PYTHON para poder obtener información de páginas WEBs, obtener textos de PDFs para luego procesarlos y mostrar en formato de nube de palabras, para lo cual crearemos un entorno de trabajo con el sistema ANACONDA y prepararemos las librerías necesarias para poder hacer trabajos con datos en formato de texto.


### Modalidad

presencial en formato taller

### Requisitos

- Una notebook con  Windows 10 o sistemas GNU/Linux
- No es necesario tener conocimientos de programación

### Objetivos generales

El objetivo general del curso es poder incorporar el uso del lenguaje de programación PYTHON a los procesos de investigación en ciencias sociales y humanas, así como aprender a buscar información y participar en las comunidades de desarrollo de dicho lenguaje.

### objetivos

- Poder instalar y configurar un entorno de trabajo PYTHON
- Entender las nociones básicas del lenguaje de programación PYTHON para poder leer y escribir código fuente
- Buscar código fuente en comunidades de desarrollo y adaptarlo a necesidades concretas de investigación
- Leer información de salida de PYTHON y depurar errores

### Contenido del curso

- Crear un entorno de trabajo con PYTHON + pip/anaconda + jupyter / THONNY / SPYDER
- Introducción a las nociones básicas del lenguaje (variables, repeticiones, si condicionales)
- Git y comunidades de desarrollo
- Obtener texto, “limpiarlo” y hacer operaciones estadísticas y gráficos de nubes de palabras con matplotlib, numpy, NLTK, PANDAS
- Manejo de errores
- Web Scraping inicial, rss, manejo de archivos pdf
