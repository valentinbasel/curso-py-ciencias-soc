#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


import feedparser
import time
# Este ejemplo es para descargar durante 24 horas, todos los titulares
# de un diario online a traves de rss
for a in range(24):
    print("#-------------------------------------------------#")
    feeds = feedparser.parse("https://www.cba24n.com.ar/rss.xml")
    print("cantidad de feeds: ",len(feeds.entries))
    print("fecha : ",feeds["feed"]["updated"])
    print("#-------------------------------------------------#")
    for dato in feeds.entries:
        print("Titulo : ",dato["title"])
    #time.sleep(3600)
    print("========================================================")
# Faltaría grabar cada entrada a un archivo
