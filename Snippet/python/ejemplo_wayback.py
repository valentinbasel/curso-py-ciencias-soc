#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# como acceder al wayback machine

from wayback import WaybackClient
import requests
from bs4 import BeautifulSoup

client = WaybackClient() # instanciamos el cliente
results = client.search('roboticaro.org') # la pagina que queremos ver
resultados = []
for pagina in results:
    print(pagina.timestamp)
    resultados.append([pagina.view_url,pagina.timestamp])


web = requests.get(resultados[0][0]) #la primer url que capturo
html = web.content.decode() #aca obtenemos el HTML de la URL
soup = BeautifulSoup(html, 'html.parser')
a = soup.find_all('p') # buscamos el tag de parrafos
cadena = ""
for b in a:
    cadena +=" "+ b.text # concatenamos todo y obtenemos una sola cadena
print(cadena)


