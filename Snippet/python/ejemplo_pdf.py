#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


import PyPDF2
lector = PyPDF2.PdfFileReader("ejemplo_radford.pdf")
print("cantidad de paginas: ",len(lector.pages))
datos = lector.getDocumentInfo()
print("autor: ",datos["/Author"])

pagina = lector.pages[0] # leemos la primer pagina
texto = pagina.extractText()

print(texto)

# para leer todas las paginas

for a in range(len(lector.pages)):
    pagina = lector.pages[a]
    texto = pagina.extractText()
    print(texto)
