#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

"""
Operaciones que se pueden hacer con archivos de texto

w = write

r = read

b = binary

a = append
"""

archivo = open("texto.txt","w")
archivo.write('hola mundo')
archivo.close()

# leer texto de un archivo
f = open ('texto.txt','r')
mensaje = f.read()
print("el texto es: " + mensaje)
f.close()

# agrega texto a nuestro archivo
f = open('texto.txt','a')
f.write('\n' + 'nuevo texto')
f.close()
#volvemos a abrirlo para ver como quedo
f = open ('texto.txt','r')
mensaje = f.read()
print("el texto es: " + mensaje)
f.close()


