#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import pandas as pd

# Crear un DataFrame
df = pd.DataFrame({
    'Nombre': ['Juan', 'María', 'Pedro'],
    'Edad': [25, 30, 35],
    'Profesión': ['Ingeniero', 'Abogada', 'Médico']
})

# Escribir el DataFrame en un archivo CSV
df.to_csv('datos_salida.csv', index=False)

# Mostrar el contenido del archivo CSV
with open('datos_salida.csv') as f:
    print(f.read())



