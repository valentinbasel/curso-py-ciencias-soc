#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 29 14:52:56 2022

@author: vbasel
"""

from wayback import WaybackClient
import requests
from bs4 import BeautifulSoup

client = WaybackClient()
results = client.search('roboticaro.org')
cadena = ""
for pagina in results:
    print(pagina.view_url,"---",pagina.timestamp)
    web = requests.get(pagina.view_url) # va a tomar la ultima url que vimos antes
    html = web.content.decode() #aca obtenemos el HTML de la URL
    soup = BeautifulSoup(html, 'html.parser')
    a = soup.find_all('p')
    for b in a:
        cadena +=" "+ b.text

print(cadena)